## How to use it?

1. Make sure to have [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html) installed and configured to you own AWS account.
2. Go IAM > Users > You > Security credentials and create a new access key to your user or use an existing one.
3. Set up your default AWS access profile by running `aws configure`. You will need the key ID and Secret from the previous step.
4. Copy the .env.example to .env and replace the BUCKET variable with your desired bucket ID.
5. Run `npm run deploy`
6. Go to S3 open your bucket and create a folder with name `input`.
7. Upload the `sample/sample-data.csv` to the `input` folder.
8. Fire AWS console and go to Lambda > Functions > s3-lambda-example-dev-newS3File > Monitor > View logs in Cloud Watch

## Clean up

1. Go to your S3 console and empty your bucket
2. Run `npm run rm`